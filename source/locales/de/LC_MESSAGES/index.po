msgid ""
msgstr ""
"Project-Id-Version: developers-reference\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-05 17:50+0000\n"
"PO-Revision-Date: 2023-04-28 20:33+0200\n"
"Last-Translator: Carsten Schoenert <c.schoenert@t-online.de>\n"
"Language-Team: \n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"Generated-By: Babel 2.10.3\n"
"X-Generator: Poedit 3.2.2\n"

#: ../index.rst:48
msgid "Appendix"
msgstr "Appendix"

#: ../index.rst:2
msgid "Debian Developer's Reference"
msgstr "Debian Entwicklerreferenz"

#: ../index.rst:4
msgid "Developer's Reference Team <developers-reference@packages.debian.org>"
msgstr "Debian Entwicklerreferenz Team <developers-reference@packages.debian.org>"

#: ../index.rst:6
msgid "Copyright © 2019 - 2023 Holger Levsen"
msgstr "Copyright © 2019 - 2023 Holger Levsen"

#: ../index.rst:7
msgid "Copyright © 2015 - 2020 Hideki Yamane"
msgstr "Copyright © 2015 - 2020 Hideki Yamane"

#: ../index.rst:8
msgid "Copyright © 2008 - 2015 Lucas Nussbaum"
msgstr "Copyright © 2008 - 2015 Lucas Nussbaum"

#: ../index.rst:9
msgid "Copyright © 2004 - 2007 Andreas Barth"
msgstr "Copyright © 2004 - 2007 Andreas Barth"

#: ../index.rst:10
msgid "Copyright © 2002 - 2009 Raphaël Hertzog"
msgstr "Copyright © 2002 - 2009 Raphaël Hertzog"

#: ../index.rst:11
msgid "Copyright © 1998 - 2003 Adam Di Carlo"
msgstr "Copyright © 1998 - 2003 Adam Di Carlo"

#: ../index.rst:12
msgid "Copyright © 1997 - 1998 Christian Schwarz"
msgstr "Copyright © 1997 - 1998 Christian Schwarz"

#: ../index.rst:14
msgid ""
"This manual is free software; you may redistribute it and/or modify it under "
"the terms of the GNU General Public License as published by the Free Software "
"Foundation; either version 2, or (at your option) any later version."
msgstr ""
"Dieses Handbuch ist freie Software, sie können es nach den Bedingungen der GNU "
"General Public License wie von der Free Software Foundation veröffentlicht, "
"Version 2 oder (nach ihrer Option) später verteilen oder verändern ."

#: ../index.rst:18
msgid ""
"This is distributed in the hope that it will be useful, but without any "
"warranty; without even the implied warranty of merchantability or fitness for a "
"particular purpose. See the GNU General Public License for more details."
msgstr ""
"Dies wird in der Hoffnung verteilt, dass es nützlich sein wird, jedoch ohne "
"jegliche Garantie; ohne auch nur die implizite Garantie der Marktgängigkeit "
"oder Eignung für einen bestimmten Zweck. Weitere Informationen finden Sie in "
"der GNU General Public License."

#: ../index.rst:22
msgid ""
"A copy of the GNU General Public License is available as /usr/share/common-"
"licenses/GPL-2 in the Debian distribution or on the World Wide Web at the GNU "
"web site. You can also obtain it by writing to the Free Software Foundation, "
"Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA."
msgstr ""
"Eine Kopie der GNU General Public License ist in der Debian Distribution "
"abgelegt unter /usr/share/common-licenses/GPL-2 oder im WWW auf der GNU "
"Webseite. Sie erhalten diese es auch schriftlich bei der Free Software "
"Foundation, Inc., 51 Franklin Street, 5. Stock, Boston, MA 02110-1301, USA."

#: ../index.rst:28
msgid ""
"This is Debian Developer's Reference version |VERSION|\\ , released on |PUBDATE|"
"\\ ."
msgstr ""
"Dies ist die Debian Entwicklerreferenz Version |VERSION|\\ , veröffentlicht am |"
"PUBDATE|\\ ."

#: ../index.rst:31
msgid ""
"If you want to print this reference, you should use the pdf version. This "
"manual is also available in some other languages."
msgstr ""
"Wenn Sie diese Referenz ausdrucken möchten, sollten Sie die PDF-Version als "
"Basis verwenden. Dieses Handbuch ist auch in einigen anderen Sprachen verfügbar."
