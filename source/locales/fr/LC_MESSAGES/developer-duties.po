# French translation of developers-reference: developer-duties
# Copyright (C) 1999-2006, 2010-2013 Debian French l10n team
# <debian-l10n-french@lists.debian.org>
# This file is distributed under the same license as the
# developers-reference package.
#
# Antoine Hulin <antoine@origan.fdn.fr>, 1999-2002.
# Frédéric Bothamy <frederic.bothamy@free.fr>, 2003-2006.
# David Prévot <david@tilapin.org>, 2010-2013.
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: developers-reference \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-03-06 08:15+0000\n"
"PO-Revision-Date: 2020-07-22 21:37+0100\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language: fr_FR\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.10.3\n"

#: ../developer-duties.rst:4
msgid "Debian Developer's Duties"
msgstr "Devoirs du développeur Debian"

#: ../developer-duties.rst:9
msgid "Package Maintainer's Duties"
msgstr "Devoirs du responsable de paquet"

#: ../developer-duties.rst:11
msgid ""
"As a package maintainer, you're supposed to provide high-quality packages"
" that are well integrated into the system and that adhere to the Debian "
"Policy."
msgstr ""
"En tant que responsable de paquet, vous êtes censé fournir des paquets de"
" haute qualité qui s'intégreront correctement dans le système et qui sont"
" conformes à la Charte Debian."

#: ../developer-duties.rst:18
msgid "Work towards the next ``stable`` release"
msgstr "Œuvrer pour la prochaine publication ``stable``"

#: ../developer-duties.rst:20
msgid ""
"Providing high-quality packages in ``unstable`` is not enough; most users"
" will only benefit from your packages when they are released as part of "
"the next ``stable`` release. You are thus expected to collaborate with "
"the release team to ensure your packages get included."
msgstr ""
"Fournir des paquets de haute qualité dans ``unstable`` ne suffit pas, la "
"plupart des utilisateurs ne profiteront de vos paquets que quand ils "
"seront publiés avec la prochaine version ``stable``. Vous êtes donc censé"
" collaborer avec l'équipe en charge de la publication pour veiller à ce "
"que vos paquets soient intégrés."

#: ../developer-duties.rst:25
msgid ""
"More concretely, you should monitor whether your packages are migrating "
"to ``testing`` (see :ref:`testing`). When the migration doesn't happen "
"after the test period, you should analyze why and work towards fixing "
"this. It might mean fixing your package (in the case of release-critical "
"bugs or failures to build on some architecture) but it can also mean "
"updating (or fixing, or removing from ``testing``) other packages to help"
" complete a transition in which your package is entangled due to its "
"dependencies. The release team might provide you some input on the "
"current blockers of a given transition if you are not able to identify "
"them."
msgstr ""
"Plus concrètement, vous devriez surveiller si vos paquets migrent vers "
"``testing`` (consultez :ref:`testing`). Lorsque la migration n'a pas lieu"
" après la période d'essai, vous devriez analyser pourquoi et œuvrer pour "
"corriger cela. Votre paquet pourrait avoir besoin d'être corrigé (dans le"
" cas de bogues critiques pour la publication ou d'échecs de construction "
"sur certaines architectures) mais cela peut également signifier mettre à "
"jour (ou corriger, ou supprimer de ``testing``) d'autres paquets pour "
"permettre de terminer une transition dans laquelle votre paquet est "
"enchevêtré à cause de ses dépendances. L'équipe en charge de la "
"publication devrait pouvoir vous renseigner sur ce qui bloque "
"actuellement une transition donnée si vous ne parvenez pas à "
"l'identifier."

#: ../developer-duties.rst:39
msgid "Maintain packages in ``stable``"
msgstr "Maintenance de paquets dans ``stable``"

#: ../developer-duties.rst:41
msgid ""
"Most of the package maintainer's work goes into providing updated "
"versions of packages in ``unstable``, but their job also entails taking "
"care of the packages in the current ``stable`` release."
msgstr ""
"La plupart du travail de responsable de paquet consiste à fournir des "
"versions de paquets mis à jour dans ``unstable``, mais son travail "
"implique aussi de s'occuper des paquets dans la publication ``stable`` "
"actuelle."

#: ../developer-duties.rst:45
msgid ""
"While changes in ``stable`` are discouraged, they are possible. Whenever "
"a security problem is reported, you should collaborate with the security "
"team to provide a fixed version (see :ref:`bug-security`). When bugs of "
"severity important (or more) are reported against the ``stable`` version "
"of your packages, you should consider providing a targeted fix. You can "
"ask the ``stable`` release team whether they would accept such an update "
"and then prepare a ``stable`` upload (see :ref:`upload-stable`)."
msgstr ""
"Même si les modifications dans ``stable`` sont déconseillées, elles sont "
"possibles. Chaque fois qu'un problème de sécurité est signalé, vous "
"devriez collaborer avec l'équipe en charge de la sécurité pour fournir "
"une version corrigée (consultez :ref:`bug-security`). Quand des bogues de"
" sévérité ``important`` (ou plus) sont soumis sur la version ``stable`` "
"de vos paquets, vous devriez envisager la possibilité de fournir une "
"correction spécifique. Vous pouvez interroger l'équipe en charge de la "
"publication ``stable`` pour savoir si elle accepterait une telle mise à "
"jour puis préparer un envoi vers ``stable`` (consultez :ref:`upload-"
"stable`)."

#: ../developer-duties.rst:57
msgid "Manage release-critical bugs"
msgstr "Gestion des bogues critiques pour la publication"

#: ../developer-duties.rst:59
msgid ""
"Generally you should deal with bug reports on your packages as described "
"in :ref:`bug-handling`. However, there's a special category of bugs that "
"you need to take care of — the so-called release-critical bugs (RC bugs)."
" All bug reports that have severity ``critical``, ``grave`` or "
"``serious`` make the package unsuitable for inclusion in the next "
"``stable`` release. They can thus delay the Debian release (when they "
"affect a package in ``testing``) or block migrations to ``testing`` (when"
" they only affect the package in ``unstable``). In the worst scenario, "
"they will lead to the package's removal. That's why these bugs need to be"
" corrected as quickly as possible."
msgstr ""
"Habituellement, vous devriez traiter les rapports de bogue sur vos "
"paquets tel que cela est décrit en :ref:`bug-handling`. Cependant, une "
"catégorie spéciale de bogues nécessite particulièrement votre attention :"
" les bogues critiques pour la publication (``release-critical`` ou "
"``RC``). Tous les rapports de bogue de gravité ``critical``, ``grave`` ou"
" ``serious`` rendent le paquet inapproprié pour être inclus dans la "
"prochaine version ``stable``. Ils peuvent donc retarder la publication de"
" Debian (quand ils concernent un paquet de ``testing``) ou bloquer des "
"migrations vers ``testing`` (quand ils concernent seulement le paquet "
"d'``unstable``). Au pire, ils pourraient conduire à la suppression du "
"paquet. C'est pourquoi ces bogues doivent être corrigés au plus tôt."

#: ../developer-duties.rst:70
msgid ""
"If, for any reason, you aren't able fix an RC bug in a package of yours "
"within 2 weeks (for example due to time constraints, or because it's "
"difficult to fix), you should mention it clearly in the bug report and "
"you should tag the bug ``help`` to invite other volunteers to chime in. "
"Be aware that RC bugs are frequently the targets of Non-Maintainer "
"Uploads (see :ref:`nmu`) because they can block the ``testing`` migration"
" of many packages."
msgstr ""
"Si pour une raison ou une autre, vous ne pouvez pas corriger un bogue "
"critique pour la publication dans un de vos paquets en moins de deux "
"semaines (par exemple à cause de contraintes de temps, ou parce que c'est"
" compliqué à corriger) vous devriez le signaler clairement dans le "
"rapport de bogue en l'étiquetant ``help`` pour encourager d'autres "
"volontaires à s'impliquer. Sachez que les bogues critiques pour la "
"publication sont souvent les cibles de mises à jour indépendantes "
"(consultez :ref:`nmu`) car ils peuvent bloquer la migration vers "
"``testing`` de plusieurs paquets."

#: ../developer-duties.rst:78
msgid ""
"Lack of attention to RC bugs is often interpreted by the QA team as a "
"sign that the maintainer has disappeared without properly orphaning their"
" package. The MIA team might also get involved, which could result in "
"your packages being orphaned (see :ref:`mia-qa`)."
msgstr ""
"Un manque d'attention aux bogues critiques pour la publication est "
"souvent considéré par l'équipe d'assurance qualité comme un signe de "
"disparition d'un responsable n'ayant pas abandonné correctement son "
"paquet. L'équipe MIA pourrait aussi s'impliquer, avec comme éventuelle "
"conséquence l'abandon de vos paquets (consultez :ref:`mia-qa`)."

#: ../developer-duties.rst:86
msgid "Coordination with upstream developers"
msgstr "Coordination avec les développeurs amont"

#: ../developer-duties.rst:88
msgid ""
"A big part of your job as Debian maintainer will be to stay in contact "
"with the upstream developers. Debian users will sometimes report bugs "
"that are not specific to Debian to our bug tracking system. These bug "
"reports should be forwarded to the upstream developers so that they can "
"be fixed in a future upstream release. Usually it is best if you can do "
"this, but alternatively, you may ask the bug submitter to do it."
msgstr ""
"Une grande part du travail de responsable Debian est de rester en contact"
" avec les développeurs amont. Parfois, les utilisateurs signalent des "
"bogues qui ne sont pas spécifiques à Debian. Ces rapports de bogue "
"doivent être transmis aux développeurs amont pour une correction dans les"
" versions suivantes. Habituellement, c'est ce qu'il y a de mieux à faire "
"si vous le pouvez, sinon, vous pouvez demander à celui qui a soumis le "
"bogue de le faire."

#: ../developer-duties.rst:95
msgid ""
"While it's not your job to fix non-Debian specific bugs, you may freely "
"do so if you're able. When you make such fixes, be sure to pass them on "
"to the upstream maintainers as well. Debian users and developers will "
"sometimes submit patches to fix upstream bugs — you should evaluate and "
"forward these patches upstream."
msgstr ""
"Bien qu'il ne soit pas de votre responsabilité de corriger les bogues non"
" spécifiques à Debian, vous pouvez le faire si vous en êtes capable. "
"Quand vous faites de telles corrections, assurez-vous de les envoyer "
"également au développeur amont. Les utilisateurs et développeurs Debian "
"proposent parfois un correctif pour les bogues amont, il vous faudra "
"alors évaluer ce correctif puis le transmettre aux développeurs amont."

#: ../developer-duties.rst:101
msgid ""
"In cases where a bug report is forwarded upstream, it may be helpful to "
"remember that the bts-link service can help with synchronizing states "
"between the upstream bug tracker and the Debian one."
msgstr ""
"Dans le cas où un rapport de bogue est transmis au développeur amont, "
"n'oubliez pas que le service ``bts-link`` peut aider à la synchronisation"
" de l'état des bogues dans le système de suivi de bogues amont et celui "
"de Debian."

#: ../developer-duties.rst:105
msgid ""
"If you need to modify the upstream sources in order to build a policy "
"compliant package, then you should propose a nice fix to the upstream "
"developers which can be included there, so that you won't have to modify "
"the sources of the next upstream version. Whatever changes you need, "
"always try not to fork from the upstream sources."
msgstr ""
"Si vous avez besoin de modifier les sources d'un logiciel pour fabriquer "
"un paquet conforme à la Charte Debian, vous devriez proposer un correctif"
" aux développeurs amont pour qu'il soit inclus dans leur version. Ainsi, "
"vous n'aurez plus besoin de modifier les sources lors des mises à jour "
"amont suivantes. Quels que soient les changements dont vous avez besoin, "
"il faut toujours essayer de rester dans la lignée des sources amont."

#: ../developer-duties.rst:111
msgid ""
"If you find that the upstream developers are or become hostile towards "
"Debian or the free software community, you may want to re-consider the "
"need to include the software in Debian. Sometimes the social cost to the "
"Debian community is not worth the benefits the software may bring."
msgstr ""
"Si vous estimez que les développeurs amont sont ou deviennent hostiles "
"envers Debian ou la communauté du logiciel libre, vous pouvez vouloir "
"reconsidérer le besoin d'inclure le logiciel dans Debian. Parfois, le "
"coût social à la communauté Debian ne vaut pas le bénéfice que le "
"logiciel peut apporter."

#: ../developer-duties.rst:117
msgid "Administrative Duties"
msgstr "Devoirs administratifs"

#: ../developer-duties.rst:119
msgid ""
"A project of the size of Debian relies on some administrative "
"infrastructure to keep track of everything. As a project member, you have"
" some duties to ensure everything keeps running smoothly."
msgstr ""
"Un projet de la taille de Debian repose sur certaines structures "
"administratives pour garder une trace de tout. En tant que membre du "
"projet, vous avez quelques devoirs pour veiller à ce que tout se déroule "
"sans problème."

#: ../developer-duties.rst:126
msgid "Maintaining your Debian information"
msgstr "Mise à jour des renseignements auprès de Debian"

#: ../developer-duties.rst:128
msgid ""
"There's a LDAP database containing information about Debian developers at"
" https://db.debian.org/\\ . You should enter your information there and "
"update it as it changes. Most notably, make sure that the address where "
"your debian.org email gets forwarded to is always up to date, as well as "
"the address where you get your debian-private subscription if you choose "
"to subscribe there."
msgstr ""
"Une base de données LDAP contient des informations sur les développeurs "
"Debian en https://db.debian.org/\\ . Vous devriez y entrer vos "
"informations et les mettre à jour quand elles changent. Le plus important"
" est de vous assurer que l'adresse vers laquelle est renvoyée le courrier"
" à destination de votre adresse debian.org est toujours à jour, de même "
"que l'adresse à laquelle vous recevez votre abonnement à debian-private "
"si vous choisissez d'être abonné à cette liste."

#: ../developer-duties.rst:135
msgid "For more information about the database, please see :ref:`devel-db`."
msgstr ""
"Pour plus d'informations sur cette base de données, veuillez consulter "
":ref:`devel-db`."

#: ../developer-duties.rst:140
msgid "Maintaining your public key"
msgstr "Gestion de clé publique"

#: ../developer-duties.rst:142
msgid ""
"Be very careful with your private keys. Do not place them on any public "
"servers or multiuser machines, such as the Debian servers (see :ref"
":`server-machines`). Back your keys up; keep a copy offline. Read the "
"documentation that comes with your software; read the `PGP FAQ "
"<http://www.cam.ac.uk.pgp.net/pgpnet/pgp-faq/>`__ and `OpenPGP Best "
"Practices <https://riseup.net/en/security/message-security/openpgp/best-"
"practices>`__."
msgstr ""
"Soyez très vigilant en utilisant votre clé privée. Ne la placez pas sur "
"un serveur public ou sur des machines multi-utilisateurs telles que les "
"serveurs Debian (voir :ref:`server-machines`). Sauvegardez vos clés et "
"gardez-en une copie hors connexion. Lisez la documentation fournie avec "
"votre logiciel, la `FAQ PGP <http://www.cam.ac.uk.pgp.net/pgpnet/pgp-"
"faq/>`__ et `OpenPGP Best Practices <https://riseup.net/fr/security"
"/message-security/openpgp/best-practices>`__."

#: ../developer-duties.rst:149
msgid ""
"You need to ensure not only that your key is secure against being stolen,"
" but also that it is secure against being lost. Generate and make a copy "
"(best also in paper form) of your revocation certificate; this is needed "
"if your key is lost."
msgstr ""
"Assurez-vous que votre clé est non seulement à l'abri des vols, mais "
"aussi d'une perte. Générez et faites une copie (c'est même mieux sur "
"papier) de votre certificat de révocation ; il est nécessaire si votre "
"clé est perdue ou volée."

#: ../developer-duties.rst:154
msgid ""
"If you add signatures to your public key, or add user identities, you can"
" update the Debian key ring by sending your key to the key server at "
"``keyring.debian.org``. Updates are processed at least once a month by "
"the ``debian-keyring`` package maintainers."
msgstr ""
"Si vous ajoutez des signatures à votre clé publique, ou des identifiants "
"d’utilisateurs, vous pouvez mettre à jour le porte-clés Debian en "
"envoyant votre clé au serveur de clefs à ``keyring.debian.org``.Les mises"
" à jour sont traitées au moins une fois par mois par les responsables du "
"paquet ``debian-keyring``."

#: ../developer-duties.rst:159
msgid ""
"If you need to add a completely new key or remove an old key, you need to"
" get the new key signed by another developer. If the old key is "
"compromised or invalid, you also have to add the revocation certificate. "
"If there is no real reason for a new key, the Keyring Maintainers might "
"reject the new key. Details can be found at "
"https://keyring.debian.org/replacing_keys.html\\ ."
msgstr ""
"Pour ajouter une nouvelle clé ou supprimer une ancienne clé, vous devez "
"faire signer la nouvelle clé par un autre développeur. Si l'ancienne clé "
"est compromise ou non valable, vous devez également ajouter le certificat"
" de révocation. S'il n'y pas de bonne raison pour une nouvelle clé, les "
"responsables du trousseau peuvent rejeter la nouvelle clé. Vous trouverez"
" plus de détails en https://keyring.debian.org/replacing_keys.html\\ ."

#: ../developer-duties.rst:166
msgid "The same key extraction routines discussed in :ref:`registering` apply."
msgstr ""
"Les mêmes routines d'extraction de clé décrites en :ref:`registering` "
"s'appliquent."

#: ../developer-duties.rst:169
msgid ""
"You can find a more in-depth discussion of Debian key maintenance in the "
"documentation of the ``debian-keyring`` package and the "
"https://keyring.debian.org/ site."
msgstr ""
"Une présentation approfondie de la gestion de clé Debian peut être "
"trouvée dans la documentation du paquet ``debian-keyring`` et sur le site"
" https://keyring.debian.org/."

#: ../developer-duties.rst:174
msgid "Voting"
msgstr "Votes"

#: ../developer-duties.rst:176
msgid ""
"Even though Debian isn't really a democracy, we use a democratic process "
"to elect our leaders and to approve general resolutions. These procedures"
" are defined by the `Debian Constitution "
"<https://www.debian.org/devel/constitution>`__."
msgstr ""
"Bien que Debian ne soit pas vraiment une démocratie, le projet utilise un"
" processus démocratique pour élire les responsables de projet et "
"approuver les résolutions générales. Ces procédures sont définies par la "
"`constitution Debian <https://www.debian.org/devel/constitution>`__."

#: ../developer-duties.rst:181
msgid ""
"Other than the yearly leader election, votes are not routinely held, and "
"they are not undertaken lightly. Each proposal is first discussed on the "
"``debian-vote@lists.debian.org`` mailing list and it requires several "
"endorsements before the project secretary starts the voting procedure."
msgstr ""
"En dehors de l'élection annuelle du responsable de projet, les votes ne "
"se tiennent pas régulièrement et ne sont pas entrepris à la légère. "
"Chaque proposition est tout d'abord discutée sur la liste de diffusion "
"``debian-vote@lists.debian.org`` et a besoin de plusieurs approbations "
"avant que le secrétaire du projet n'entame la procédure de vote."

#: ../developer-duties.rst:186
#, fuzzy
msgid ""
"You don't have to track the pre-vote discussions, as the secretary will "
"issue several calls for votes on ``debian-devel-"
"announce@lists.debian.org`` (and all developers are expected to be "
"subscribed to that list). Democracy doesn't work well if people don't "
"take part in the vote, which is why we encourage all developers to vote. "
"Voting is conducted via OpenPGP-signed/encrypted email messages."
msgstr ""
"Vous n'avez pas besoin de suivre les discussions précédant le vote car le"
" secrétaire enverra plusieurs appels au vote sur la liste ``debian-devel-"
"announce@lists.debian.org`` (et tous les développeurs devraient être "
"inscrits à cette liste). La démocratie ne fonctionne pas si les personnes"
" ne prennent pas part au vote, c'est pourquoi nous encourageons tous les "
"développeurs à voter. Le vote est conduit par messages signés ou chiffrés"
" par GPG."

#: ../developer-duties.rst:194
msgid ""
"The list of all proposals (past and current) is available on the `Debian "
"Voting Information <https://www.debian.org/vote/>`__ page, along with "
"information on how to make, second and vote on proposals."
msgstr ""
"La liste de toutes les propositions (passées et présentes) est disponible"
" sur la page des `informations sur les votes Debian "
"<https://www.debian.org/vote/>`__, ainsi que des informations "
"complémentaires sur la procédure à suivre pour effectuer une proposition,"
" la soutenir et voter pour elle."

#: ../developer-duties.rst:201
msgid "Going on vacation gracefully"
msgstr "Départ en vacances poli"

#: ../developer-duties.rst:203
msgid ""
"It is common for developers to have periods of absence, whether those are"
" planned vacations or simply being buried in other work. The important "
"thing to notice is that other developers need to know that you're on "
"vacation so that they can do whatever is needed if a problem occurs with "
"your packages or other duties in the project."
msgstr ""
"Il est courant que les développeurs s'absentent, que ce soit pour des "
"vacances prévues ou parce qu'ils sont submergés de travail. L'important "
"est que les autres développeurs ont besoin de savoir si vous êtes "
"indisponible pour pouvoir agir en conséquence si un problème se produit "
"sur vos paquets ou autre pendant votre absence."

#: ../developer-duties.rst:209
msgid ""
"Usually this means that other developers are allowed to NMU (see "
":ref:`nmu`) your package if a big problem (release critical bug, security"
" update, etc.) occurs while you're on vacation. Sometimes it's nothing as"
" critical as that, but it's still appropriate to let others know that "
"you're unavailable."
msgstr ""
"Habituellement, cela signifie que les autres développeurs peuvent faire "
"des NMU (voir :ref:`nmu`) sur votre paquet si un gros problème (bogue "
"empêchant l'intégration dans la distribution, mise à jour de sécurité, "
"etc.) se produit pendant que vous êtes en vacances. Parfois, ce n'est pas"
" très important, mais il est de toute façon approprié d'indiquer aux "
"autres que vous n'êtes pas disponible."

#: ../developer-duties.rst:215
msgid ""
"In order to inform the other developers, there are two things that you "
"should do. First send a mail to ``debian-private@lists.debian.org`` with "
"[VAC] prepended to the subject of your message [1]_ and state the period "
"of time when you will be on vacation. You can also give some special "
"instructions on what to do if a problem occurs."
msgstr ""
"Il y a deux choses à faire pour informer les autres développeurs. "
"Premièrement, envoyez un courrier électronique à ``debian-"
"private@lists.debian.org`` en commençant le sujet de votre message par « "
"[VAC] » [1]_ et donnez la période de vos vacances. Vous pouvez également "
"donner quelques instructions pour indiquer comment agir si un problème "
"survenait."

#: ../developer-duties.rst:221
msgid ""
"The other thing to do is to mark yourself as on vacation in the :ref"
":`devel-db` (this information is only accessible to Debian developers). "
"Don't forget to remove the on vacation flag when you come back!"
msgstr ""
"L'autre chose à faire est de vous signaler comme en vacances (``on "
"vacation``) dans la :ref:`devel-db` (l'accès à cette information est "
"réservé aux développeurs). N'oubliez pas de retirer l'indicateur ``on "
"vacation`` à votre retour !"

#: ../developer-duties.rst:225
#, fuzzy
msgid ""
"Ideally, you should sign up at the `OpenPGP coordination pages "
"<https://wiki.debian.org/Keysigning>`__ when booking a holiday and check "
"if anyone there is looking for signing. This is especially important when"
" people go to exotic places where we don't have any developers yet but "
"where there are people who are interested in applying."
msgstr ""
"Dans l'idéal, vous devriez vous connecter sur les `pages de coordination "
"GPG <https://wiki.debian.org/Keysigning>`__ quand vous prévoyez un départ"
" et vérifier si quelqu'un recherche un échange de signatures. Cela est "
"particulièrement important quand des personnes vont à des endroits "
"exotiques où nous n'avons pas encore de développeurs, mais où il y a des "
"personnes intéressées pour poser leur candidature."

#: ../developer-duties.rst:235
msgid "Retiring"
msgstr "Démission"

#: ../developer-duties.rst:237
msgid ""
"If you choose to leave the Debian project, you should make sure you do "
"the following steps:"
msgstr ""
"Si vous décidez de quitter le projet Debian, veillez à procéder comme "
"suit :"

#: ../developer-duties.rst:240
msgid "Orphan all your packages, as described in :ref:`orphaning`."
msgstr "Abandonnez tous vos paquets comme décrit en :ref:`orphaning` ;"

#: ../developer-duties.rst:242
msgid "Remove yourself from uploaders for co- or team-maintained packages."
msgstr ""
"Retirez-vous des téléverseurs des paquets dont vous êtes co-responsable "
"ou responsable en équipe."

#: ../developer-duties.rst:244
msgid ""
"If you received mails via a @debian.org e-mail alias (e.g. "
"press@debian.org) and would like to get removed, open a RT ticket for the"
" Debian System Administrators. Just send an e-mail to "
"``admin@rt.debian.org`` with \"Debian RT\" somewhere in the subject "
"stating from which aliases you'd like to get removed."
msgstr ""
"Si vous recevez des courriels d’alias d’adresse @debian.org (p. ex. "
"press@debian.org) et ne le désirez plus, ouvrez un ticket ``RT`` pour les"
" administrateurs du système Debian (``Debian System Administrators``). "
"Écrivez simplement à ``admin@rt.debian.org`` avec « Debian RT » dans le "
"sujet et en déclarant de quel alias vous voulez être supprimé."

#: ../developer-duties.rst:250
msgid ""
"Please remember to also retire from teams, e.g. remove yourself from team"
" wiki pages or salsa groups."
msgstr ""
"N'oubliez pas de vous retirer des équipes, par exemple des pages wiki des"
" équipes ou des groupes de sala."

#: ../developer-duties.rst:253
msgid ""
"Use the link https://nm.debian.org/process/emeritus to log in to "
"nm.debian.org, request emeritus status and write a goodbye message that "
"will be automatically posted on debian-private."
msgstr ""
"Connectez-vous à nm.debian.org en utilisant le lien "
"https://nm.debian.org/process/emeritus, demandez le statut émérite et "
"écrivez un message d'adieu qui sera envoyé automatiquement à la liste "
"``debian-private``."

#: ../developer-duties.rst:257
#, fuzzy
msgid ""
"Authentication to the NM site requires an SSO browser certificate. You "
"can generate them on https://sso.debian.org."
msgstr ""
"Un certificat d'authentification unique sur le navigateur est requis pour"
" le site des nouveaux membres. Vous pouvez en créer un sur "
"https://sso.debian.org."

#: ../developer-duties.rst:260
msgid ""
"In the case you run into problems opening the retirement process "
"yourself, contact NM front desk using ``nm@debian.org``"
msgstr ""
"Au cas où vous rencontrez des difficultés pour initier le processus de "
"démission, contactez le secrétariat des nouveaux membres (``NM Front "
"Desk``) par un message à ``nm@debian.org``."

#: ../developer-duties.rst:263
msgid ""
"It is important that the above process is followed, because finding "
"inactive developers and orphaning their packages takes significant time "
"and effort."
msgstr ""
"Le processus précédemment décrit devrait absolument être suivi, car "
"trouver les développeurs inactifs et déclarer orphelins leurs paquets est"
" une tâche longue et fastidieuse."

#: ../developer-duties.rst:270
msgid "Returning after retirement"
msgstr "Revenir après démission"

#: ../developer-duties.rst:272
msgid ""
"A retired developer's account is marked as \"emeritus\" when the process "
"in :ref:`s3.7` is followed, and \"removed\" otherwise. Retired developers"
" with an \"emeritus\" account can get their account re-activated as "
"follows:"
msgstr ""
"Le compte d'un développeur est marqué « emeritus » (honoraire) quand le "
"processus précédent en :ref:`s3.7` est suivi, et « removed » (supprimé) "
"sinon. Un développeur ayant démissionné avec un compte « emeritus » peut "
"réactiver son compte de la façon suivante :"

#: ../developer-duties.rst:277
#, fuzzy
msgid ""
"Get access to an salsa account (either by remembering the credentials for"
" your old guest account or by requesting a new one as described at `SSO "
"Debian wiki page "
"<https://wiki.debian.org/DebianSingleSignOn#If_you_ARE_NOT_.28yet.29_a_Debian_Developer>`__."
msgstr ""
"Obtenez l'accès à un compte alioth (soit en vous rappelant l'identifiant "
"de votre ancien compte client, soit en en demandant un nouveau comme cela"
" est décrit sur la page SSO du wiki de Debian "
"<https://wiki.debian.org/DebianSingleSignOn#If_you_ARE_NOT_.28yet.29_a_Debian_Developer>`__."

#: ../developer-duties.rst:282
msgid "Mail ``nm@debian.org`` for further instructions."
msgstr ""
"Pour obtenir davantage d'instructions, envoyez un message à "
"``nm@debian.org``."

#: ../developer-duties.rst:284
msgid ""
"Go through a shortened NM process (to ensure that the returning developer"
" still knows important parts of P&P and T&S)."
msgstr ""
"Passer un processus raccourci de nouveau responsable (pour s'assurer "
"qu'il connaît toujours les parties importantes de « philosophie et "
"procédures » et « tâches et compétences »)."

#: ../developer-duties.rst:287
msgid ""
"Retired developers with a \"removed\" account need to go through full NM "
"again."
msgstr ""
"Les développeurs ayant démissionné avec un compte « removed » doivent "
"repasser le processus complet de nouveau membre."

#: ../developer-duties.rst:291
msgid ""
"This is so that the message can be easily filtered by people who don't "
"want to read vacation notices."
msgstr ""
"Ainsi, le message peut être facilement filtré par les personnes qui ne "
"veulent pas lire ces annonces de vacances."

#~ msgid ""
#~ "Send an gpg-signed email announcing "
#~ "your retirement to ``debian-"
#~ "private@lists.debian.org``."
#~ msgstr ""
#~ "Envoyez un courrier électronique signé "
#~ "par GnuPG à ``debian-"
#~ "private@lists.debian.org`` annonçant votre abandon"
#~ " ;"

#~ msgid ""
#~ "Notify the Debian key ring maintainers"
#~ " that you are leaving by opening "
#~ "a ticket in Debian RT by sending"
#~ " a mail to ``keyring@rt.debian.org`` with"
#~ " the words \"Debian RT\" somewhere in"
#~ " the subject line (case doesn't "
#~ "matter)."
#~ msgstr ""
#~ "Signalez aux responsables du porte-clés"
#~ " Debian que vous quittez le projet"
#~ " en ouvrant un ticket en écrivant "
#~ "à ``keyring@rt.debian.org`` avec les mots "
#~ "« Debian RT » dans le sujet "
#~ "(peu importe la casse) ;"

#~ msgid "Contact ``da-manager@debian.org``."
#~ msgstr "contacter ``da-manager@debian.org`` ;"

#~ msgid ""
#~ "Prove that they still control the "
#~ "GPG key associated with the account, "
#~ "or provide proof of identify on a"
#~ " new GPG key, with at least two"
#~ " signatures from other developers."
#~ msgstr ""
#~ "démontrer qu'il possède toujours la clef"
#~ " GPG associée au compte, ou fournir"
#~ " des preuves d'identité sur une "
#~ "nouvelle clef GPG, avec au moins "
#~ "deux signatures d'autres développeurs Debian."

